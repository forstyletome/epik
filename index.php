<?
require "config.php";
require "vendor/autoload.php";

use Models\Db;
use Controllers\UsersController;

$db = new Db();

$users = new UsersController();
$users->execute();

/*
	
	Для общего понимания оставлю тут описание того, что сделал.
	
	Задача звучала так:
	
	"Задача: написать класс, который решает проблему дублирования записей профилей пользователей. Как пример - можно использовать плоскую таблицу записей пользователей. 
	Мы говорим о уже имеющейся таблице с профилями пользователей в которой уже есть дубликаты и нужно данные дубликаты устранить. 
	Количество записей в таблице - 10 000."
	
	Для работы с БД решил использовать https://github.com/illuminate/database
	Ранее с laravel не сталкивался, за одно решил немного попробовать.
	
	В UsersController три метода:
		
		1. getDuplicateUsers - Получение всех записей в таблице с подсчётом количества дубликатов разбивая по 25 результатов на страницу. Уникальными по умолчанию считаю EMAIL и TELEPHONE. Запрос:
		*** select `ID`, `NAME`, `LAST_NAME`, `TELEPHONE`, `EMAIL`, count(*) as CNT from `user` group by `EMAIL`, `TELEPHONE` having count(*) > 1 order by `CNT` asc ***
		
		2. withoutDuplicateUsers - Получение всех записей в таблице разбивая по 25 результатов на страницу. Уникальными по умолчанию считаю EMAIL и TELEPHONE. Запрос:
		*** select `ID`, `NAME`, `LAST_NAME`, `TELEPHONE`, `EMAIL`, count(*) as CNT from `user` group by `EMAIL`, `TELEPHONE` having count(*) > 1 order by `CNT` asc ***
		
		3. cleanDuplicateUsers - Создаёт копию таблицы user (user_copy) и заполняет её без дублей. Запрос - *** INSERT INTO `user_copy` SELECT `ID`, `NAME`, `LAST_NAME`, `TELEPHONE`, `EMAIL` FROM `user` GROUP BY `EMAIL`, `TELEPHONE` ***	
	
	По сути очисткой от дублей занимается метод №3.
	
*/
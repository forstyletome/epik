<?
namespace Controllers;

use Illuminate\Http\Request;

use Models\UsersDuplicateModel;
use Models\UsersCleanDuplicateModel;
use Models\UsersWitoutDuplicateModel;
use Models\ViewModel;
use Models\NavigationModel;

class UsersController{
	
	public static function getDuplicateUsers(){
		
		$result = [];
		
			$modelUsersDuplicate = new UsersDuplicateModel();
			$modelView = new ViewModel();
		
			$data = $modelUsersDuplicate->requestDuplicateUsers();
			
			$result["DATA"] = $modelView->renderViewDuplicateUsers($data["data"]);
			$result["PAGINATION"] = $modelView->renderPagination($data["links"]);
			
		return $result;
		
	}
	
	public function cleanDuplicateUsers(){
		
		$modelUsersCleanDuplicate = new UsersCleanDuplicateModel();
		$modelUsersCleanDuplicate->requestCleanDuplicateUsers();
		
	}
	
	public function getNavigation(){
		
		$model = new NavigationModel();
		
		return $model->getNavigation();
		
	}
	
	public function withoutDuplicateUsers(){
		
		$modelUsersWitoutDuplicate = new UsersWitoutDuplicateModel();
		$modelView = new ViewModel();
		
		$data = $modelUsersWitoutDuplicate->requestWithoutDuplicateUsers();
		
		$result["DATA"] = $modelView->renderViewDuplicateUsers($data["data"]);
		$result["PAGINATION"] = $modelView->renderPagination($data["links"]);
			
		return $result;
		
	}
	
	public function execute(){
		
		$request = new Request();
		$request = $request->capture();
		
		echo $this->getNavigation();
		
		switch($request->query("type")){
			
			case "clean_duplicates":
				
				$this->cleanDuplicateUsers();
				
				echo "OK"; // Не стал замарачиваться с выводом исключений в случае ошибки. По задаче 10к должно и так очень быстро отработать.
				
			break;
			
			case "without_duplicates":
				
				$data = $this->withoutDuplicateUsers();
				
				echo $data["DATA"], 
					 $data["PAGINATION"];

			break;
			
			case "get_all_duplicates":
			default:
				
				$data = $this->getDuplicateUsers();
				
				echo $data["DATA"], 
					 $data["PAGINATION"];
				
			break;
			
		}
		
	}

}
<?
namespace Models;
	
use Illuminate\Database\Eloquent\Model;

class UsersDuplicateModel extends Model{
	
	protected $table = "user";
	protected $fillable = [];
	public $timestamps = false;
	public $pagination = 25;
	
	public function requestDuplicateUsers(){
		
		\Illuminate\Pagination\Paginator::currentPageResolver(function($pageName = 'page'){
			return (int) ($_GET[$pageName] ?? 1);
		});
		
		$data = UsersDuplicateModel::select(
				'ID',
				'NAME',
				'LAST_NAME',
				'TELEPHONE',
				'EMAIL',
				UsersDuplicateModel::raw('count(*) as CNT')
			)
			->groupBy(
				'EMAIL', 
				'TELEPHONE'
			)
			->havingRaw(
				'count(*) > 1'
			)
			->orderBy(
				'CNT'
			)
			->paginate($this->pagination)
			->appends(['type' => 'get_all_duplicates'])
			->toArray();
	
		return $data;
		
	}
	
}
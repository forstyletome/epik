<?
namespace Models;

class NavigationModel{
	
	public function getNavigation(){
		
		$view = "";
		
		$view.= "<ul>";
			$view.= "<li><a href='/?type=get_all_duplicates'>Показать количество дубликатов</a></li>";
			$view.= "<li><a href='/?type=clean_duplicates'>Удалить дубликаты</a></li>";
			$view.= "<li><a href='/?type=without_duplicates'>Показать очищенную таблицу</a></li>";
		$view.= "</ul>";
		
		return $view;
		
	}
	
}
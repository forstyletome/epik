<?
namespace Models;
	
use Illuminate\Database\Eloquent\Model;

class UsersWitoutDuplicateModel extends Model{
	
	protected $table = "user_copy";
	protected $fillable = [];
	public $timestamps = false;
	public $pagination = 25;
	
	public function requestWithoutDuplicateUsers(){
		
		\Illuminate\Pagination\Paginator::currentPageResolver(function($pageName = 'page'){
			return (int) ($_GET[$pageName] ?? 1);
		});
		
		$data = UsersWitoutDuplicateModel::select(
				'ID',
				'NAME',
				'LAST_NAME',
				'TELEPHONE',
				'EMAIL',
				UsersWitoutDuplicateModel::raw('count(*) as CNT')
			)
			->groupBy(
				'EMAIL', 
				'TELEPHONE'
			)
			->orderBy(
				'CNT'
			)
			->paginate($this->pagination)
			->appends(['type' => 'without_duplicates'])
			->toArray();
			
		return $data;
		
	}
	
}
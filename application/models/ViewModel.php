<?
namespace Models;

class ViewModel{
	
	public function renderViewDuplicateUsers($data){
		
		$view = "";
		
		$view.= "<table>";
			$view.= "<tr>";
				$view.= "<td>ID</td>";
				$view.= "<td>NAME</td>";
				$view.= "<td>LAST_NAME</td>";
				$view.= "<td>TELEPHONE</td>";
				$view.= "<td>EMAIL</td>";
				$view.= "<td>Количество</td>";
			$view.= "</tr>";
		
		foreach($data as $item){
			
			$view.= "<tr>";
				$view.= "<td>".$item["ID"]."</td>";
				$view.= "<td>".$item["NAME"]."</td>";
				$view.= "<td>".$item["LAST_NAME"]."</td>";
				$view.= "<td>".$item["TELEPHONE"]."</td>";
				$view.= "<td>".$item["EMAIL"]."</td>";
				$view.= "<td>".$item["CNT"]."</td>";
			$view.= "</tr>";

		}
		
		$view.= "</table>";
		
		return $view;
		
	}
	
	public function renderPagination($data){

		$view = "";
		
		$view.= "<ul>";
		
		foreach($data as $item){
			
			if(!empty($item["url"])){
			
				$view.= "<li style='display:inline; padding:10px;'><a href=".$item["url"].">".$item["label"]."</a></li>";
			
			}else{
				
				$view.= "<li>".$item["label"]."</li>";
				
			}
			
		}
		
		$view.= "</ul>";
		
		return $view;
		
	}
	
}
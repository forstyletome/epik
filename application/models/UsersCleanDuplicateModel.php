<?
namespace Models;
	
use Illuminate\Database\Capsule\Manager as Capsule;

class UsersCleanDuplicateModel{
	
	public function requestCleanDuplicateUsers(){
		
		if(!Capsule::schema()->hasTable('user_copy')){
			
			Capsule::schema()->create('user_copy', function($table){
				$table->increments('ID');
				$table->string('NAME', 255)->nullable();
				$table->string('LAST_NAME', 255)->nullable();
				$table->string('TELEPHONE', 255)->nullable();
				$table->string('EMAIL', 255)->nullable();
			});
			
			Capsule::statement('INSERT INTO `user_copy` SELECT `ID`, `NAME`, `LAST_NAME`, `TELEPHONE`, `EMAIL` FROM `user` GROUP BY `EMAIL`, `TELEPHONE`');
			
		}else{
			
			Capsule::table('user_copy')->truncate();
			
			Capsule::statement('INSERT INTO `user_copy` SELECT `ID`, `NAME`, `LAST_NAME`, `TELEPHONE`, `EMAIL` FROM `user` GROUP BY `EMAIL`, `TELEPHONE`');
			
		}		
		
	}
	
}